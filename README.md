<h1 align="center">Todo-list React interface</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="./out/index.html" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
</p>

> Simple todo-list interface created with React.js

## Install

```sh
npm install
```

## Usage

```sh
Run the index.html in ./build or use npm run dev
```

## Author

👤 **Kirill**

* Website: keria132.github.io
* Github: [@keria132](https://github.com/keria132)
* LinkedIn: [@kirill-mikeilov](https://linkedin.com/in/kirill-mikeilov)

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_