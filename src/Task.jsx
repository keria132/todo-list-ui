/** Task module
 * @module Task
 */
import React from 'react';
import { useState, useEffect } from 'react';
import doneIcon from "./assets/done-icon.svg"
import editIcon from "./assets/edit-icon.svg"
import deleteIcon from "./assets/delete-icon.svg"
import fileIcon from "./assets/file-icon.svg"

function Task(props){

    /** useState value to switch the task between normal and edit modes */
    const [editMode, setEditMode] = useState(false);

    /**
     * Function that allows us to edit the task if we were in edit mode
     * @function handleEdit
     * @param {*} e - event that triggered the function
     */
    function handleEdit(e){
        if(editMode === true){
            let editedTask = {
                name: e.target.parentElement.children[0].value,
                description: e.target.parentElement.children[4].value,
                date: e.target.parentElement.children[5].children[1].value,
                file: e.target.parentElement.children[2].files.length === 0 ? "No file" : e.target.parentElement.children[2].files[0].name,
                status: ''
            }
            /** Use callback function to set edited task in taskList */
            props.editHandler(props.index, editedTask)
        }
        /** Switch the mode */
        setEditMode(!editMode)
    }

    /** Function to change task status
     * @function handleComplete
     */
    function handleComplete(){
        if(props.date < dayjs().format('YYYY-MM-DD')){
            /** If task is "overdue" then you cant set "complete" status */
            return
        }
        if(props.status == "complete"){
            /** If task is "complete" change it to "" */
            props.statusHandler(props.index, "");
            return;
        }
        /** Using callback function to change task status */
        props.statusHandler(props.index, "complete")
    }

    /** Return html that allows us to edit the task if in edit mode */
    if(editMode === true){
        return(
            <div className='task task_edit'>
                <input type="text" className='task__name_edit' defaultValue={props.name}/>
                <img className="task__button_edit" src={doneIcon} onClick={handleEdit} />
                <input type="file" id="files" style={{display: "none"}} />
                <img className='task__file-icon' onClick={(e) => {e.target.parentElement.children[2].click()}} src={fileIcon}/>
                <textarea className='task__description_edit' defaultValue={props.description}></textarea>

                <div className='task__date_edit'>
                    <label htmlFor={props.index}>Pick a date:</label>
                    <input type="date" id={props.index} defaultValue={props.date}/>
                </div>
            </div>
        )
    }

    return(
        <div className='task' style={{ opacity: props.date < dayjs().format('YYYY-MM-DD') ? 0.5 : 1 }}>
            <h3 className='task__name'>{props.name}</h3>

            <div className='task__icons'>
                <img className='task__icon' src={doneIcon} alt="done" onClick={handleComplete} style={{backgroundColor: props.status == "complete" ? "#80ed99" : "#3d5a80"}}/>
                <img className='task__icon' src={editIcon} onClick={handleEdit} alt="edit" />
                <img className='task__icon' src={deleteIcon} alt="deldete" onClick={() => {
                    /** Using callback function to delete the task */
                    props.deleteHandler(props.index)
                }} />
            </div>

            <p className='task__description'>{props.description}</p>
            
            <input className='task__date' type="date" id="date" defaultValue={props.date} disabled/>
            <p className='task__file'>{props.file}</p>
        </div>
    )
}

export default Task;