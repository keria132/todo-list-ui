/** Main App */
import React from 'react';
import { useState } from 'react'
import './App.less'
import Task from './Task';
import filesIcon from "./assets/file-icon.svg"

function App() {
    /** Get current time from dayjs */
    let today = dayjs().format('YYYY-MM-DD')

    /** Task list which contains all tasks */
    const [taskList, setTaskList] = useState([])

    /**
     * Creates a new task object and sets it in the array
     * @function handleNewTask
     * @param {*} e - event that triggered function
     */
    function handleNewTask(e){
        let newTask = {
            name: e.target.parentElement.children[1].value,
            description: e.target.parentElement.children[3].children[1].value,
            date: e.target.parentElement.children[4].children[0].value,
            file: e.target.parentElement.children[5].files.length === 0 ? 'No file' : e.target.parentElement.children[5].files[0].name,
            status: ''
        }
        console.log(newTask);

        e.target.parentElement.children[1].value = "New Task";
        e.target.parentElement.children[3].children[1].value = "Some text";
        e.target.parentElement.children[4].children[0].value = today;
        /** Set new task in the taskList array */
        setTaskList([...taskList, newTask]);
    }

    /**
     * Callback function that we pass as <Task> props in order to change targeted task variables
     * @callback handleTaskEdit
     * @param {number} taskIndex - Indicates the task index which we want to edit in array
     * @param {object} editedTask - Edited task with new variables
     */
    function handleTaskEdit(taskIndex, editedTask){
        /**Copy taskList array in order to change targeted task */
        let newTaskList = [...taskList];
        newTaskList[taskIndex] = editedTask;
        console.log("New task list: ", newTaskList)
        /**Set the new task array after changing the targeted task */
        setTaskList(newTaskList)
    }

    /**
     * Callback function that we pass in <Task> props in order to delete the task
     * @callback handleDeleteTask
     * @param {number} taskIndex - Indicates the task index which we want to delete in array
     */
    function handleDeleteTask(taskIndex){
        /**Copy taskList array and delete targeted task */
        let newTaskList = [...taskList];
        newTaskList.splice(taskIndex, 1);
        console.log("New task list: ", newTaskList)
        /**Set the new task array after deleting the targeted task */
        setTaskList(newTaskList);
    }

    /**
     * Callback function that we pass in <Task> props in order to change task status
     * @callback handleTaskStatus
     * @param {number} taskIndex - Task index in array
     * @param {string} status - Task new status
     */
    function handleTaskStatus(taskIndex, status){
        let newTaskList = [...taskList];
        newTaskList[taskIndex].status = status;
        console.log("New task list: ", newTaskList)
        /**Set the new task array after changing the task status */
        setTaskList(newTaskList);
    }

    return (
        <main>
            <section className='taskInput'>
                <h2 className='taskInput__heading'>New task:</h2>

                <input className="taskInput__text" type="text" defaultValue="New Task"/>
                <button className="taskInput__button" onClick={handleNewTask}>CREATE</button>

                <div className='taskInput__textarea'>
                    <label htmlFor="description">Description:</label>
                    <textarea id="description" defaultValue="Some text">
                    
                    </textarea>
                </div>

                <div className='taskInput__date'>
                    <input type="date" id="date" defaultValue={today}/>
                </div>

                <input type="file" id="files" style={{display: "none"}} />
                <img className='taskInput__files' onClick={(e) => {e.target.parentElement.children[5].click()}} src={filesIcon}/>
            </section>

            <section className='taskList'>
                {taskList.map((task, index) => {
                    return <Task key={index} index={index} name={task.name} description={task.description} date={task.date} status={task.status} file={task.file}
                    /** Passing edit callback
                     * @param {handleTaskEdit} - Edit callback
                    */
                    editHandler={handleTaskEdit}

                    /** Passing delete callback
                     * @param {handleDeleteTask} - Delete callback
                    */
                    deleteHandler={handleDeleteTask}

                    /** Passing status changing callback
                     * @param {handleTaskStatus} - Status callback
                    */
                    statusHandler={handleTaskStatus}
                    />
                })}
            </section>
        </main>
    )
}

export default App